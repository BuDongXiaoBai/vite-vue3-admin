FROM node:latest

WORKDIR /dist

COPY . .

RUN npm config set registry https://registry.npmmirror.com/

RUN npm install -g http-server

EXPOSE 9999

CMD ["http-server", "-p", "9999"]
